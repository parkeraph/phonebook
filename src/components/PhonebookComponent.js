import React from 'react';

const Entry = ({name, number, deleteHandler}) => {
    return(
        <li>
            <div className="infoContainers">{name}: {number}  </div>
            <button className="deleteButtons" onClick={deleteHandler}>delete</button>
        </li> 
    )
}

export default Entry;