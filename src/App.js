import React, { useState, useEffect } from 'react'
import phonebookService from './services/phonebookService'
import Entry from './components/PhonebookComponent'

const App = () => {
  const [ entries, setEntries] = useState([]) 
  const [ newName, setNewName ] = useState('')
  const [ newNumber, setNewNumber ] = useState('')

  const initFetch  = () =>  {
    phonebookService.getAll().then(recievedEntries => {
      setEntries(recievedEntries);
    })
  }

  useEffect(initFetch, [])

  const handleDelete = id => {
    phonebookService.deleteEntry(id).then(returnedData => {
      console.log(id, "deleted");
      setEntries(entries.filter(entry => entry.id !== id));
    }).catch(err => {
      alert("Could not delete item!");
    })

  }

  const entryList = entries.map(thisPerson => <Entry
     name={thisPerson.name} 
     number={thisPerson.number}  
     key={thisPerson.id} 
     deleteHandler={() => {handleDelete(thisPerson.id)}} 
     />)

  const  handleSubmit = (event) => {
    event.preventDefault()

    //check if person exists
    if((entries.find(person => {return  person.name === newName})) !== undefined){
      alert("person already exists");
      return
    }

    const newPersonObj = {
      name: newName,
      number:  newNumber,
      id: entries.length+1
    }

    phonebookService.newEntry(newPersonObj).then(returnedEntry => {
      setEntries(entries.concat(newPersonObj));
      setNewName('')
      setNewNumber('')
    })
  }

  const handleNewNameChange = (event) => {
    //console.log(event.target.value);
    setNewName(event.target.value);
  }

  const handleNewNumberChange = (event) => {
    //console.log(event.target.value);
    setNewNumber(event.target.value);
  }

  return (
    <div>
      
      <h2>New Entry</h2>
        <form onSubmit={handleSubmit}>
          <div>
            name: <input value={newName} onChange={handleNewNameChange} />
          </div>
          <div>
            number: <input value={newNumber} onChange={handleNewNumberChange} />
          </div>
          <div>
            <button  type="submit">add</button>
          </div>
        </form>
      <h2>Numbers</h2>
      <ul>
        {entryList}
      </ul>
    </div>
  )
}

export default App