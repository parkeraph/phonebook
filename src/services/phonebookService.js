import axios from 'axios';

const baseURL = "http://localhost:3001/"

const getAll = () => {
   console.log("Get All Entries")
   const promise = axios.get(baseURL+"db")
   return promise.then(resp => resp.data.persons)
}

const newEntry = (newObj) => {
    console.log("New Entry")
    const promise = axios.post(baseURL+"persons", newObj);
    return promise.then(resp => resp.data);
} 

const deleteEntry = (id) => {
    console.log("Delete Entry"+id);
    const promise = axios.delete(baseURL+"persons/"+id);
    return promise.then(resp => resp.data);
}

export default {
    getAll,
    newEntry,
    deleteEntry
}